package Repository.jpa;

import model.Tariff;
import model.TariffState;
import model.forDAO.TariffService;
import model.forDAO.TariffStateService;

import java.util.List;

/**
 * Created by Евгений on 24.10.2016.
 */
public class TariffStateRepository {

    private List<TariffState> query;
    private TariffStateService service;


    public TariffStateRepository(TariffStateService entityManager) {
        service = entityManager;
        query = entityManager.getTariffStates();
    }


    public List<TariffState> getQuery()
    {
        return query;
    }


}