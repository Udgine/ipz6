package Repository.jpa;

import Repository.IContactRepository;
import model.Contact;
import model.forDAO.BalanceService;
import model.forDAO.ContactService;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class ContactRepository implements IContactRepository {
    private List<Contact> query;
    private ContactService service;


    public ContactRepository ( ContactService entityManager )
    {
        service = entityManager;
        query = entityManager.getContacts();
    }



    @Override
    public Contact findByAdress (String adress )
    {
        List<Contact>  res =service.getContactByAddress(adress);
        return ( res.size() == 1 ) ? res.get( 0 ) : null;
    }

    @Override
    public List<Contact> getQuery()
    {
        return query;
    }

}
