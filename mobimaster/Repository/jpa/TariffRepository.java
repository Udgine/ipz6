package Repository.jpa;

import Repository.ITariffRepository;
import model.ConnectedServices;
import model.Service;
import model.Tariff;
import model.forDAO.ConnectedServicesService;
import model.forDAO.TariffService;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class TariffRepository implements ITariffRepository {
    private List<Tariff> query;
    private TariffService service;


    public TariffRepository ( TariffService entityManager )
    {
        service = entityManager;
        query = entityManager.getTariffs();
    }

    @Override
    public List<Tariff> getQuery()
    {
        return query;
    }


    @Override
    public Tariff findByName (String name )
    {
        List<Tariff>  res =service.getTariffByName(name);
        return ( res.size() == 1 ) ? res.get( 0 ) : null;
}
    }
