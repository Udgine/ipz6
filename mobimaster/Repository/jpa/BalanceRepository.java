package Repository.jpa;

import Repository.IBalanceRepository;
import model.Account;
import model.Balance;
import model.forDAO.BalanceService;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class BalanceRepository implements IBalanceRepository {
    private List<Balance> query;
    private BalanceService service;


    public BalanceRepository ( BalanceService entityManager )
    {
        service = entityManager;
        query = entityManager.getBalances();
    }

    @Override
    public List<Balance> getQuery ()
    {
        return query;
    }

    @Override
    public Balance findByValue ( double value )
    {
        List<Balance>  res =service.getBalanceByValue(value);
        return ( res.size() == 1 ) ? res.get( 0 ) : null;
    }

}
