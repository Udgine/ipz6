package Repository.jpa;

import DTO.InternetInfoDTO;
import Repository.IInternetInfoRepository;
import model.Contact;
import model.InternetInfo;
import model.forDAO.ContactService;
import model.forDAO.InternetInfoService;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class InternetInfoRepository implements IInternetInfoRepository{

    private List<InternetInfo> query;
    private InternetInfoService service;


    public InternetInfoRepository ( InternetInfoService entityManager )
    {
        service = entityManager;
        query = entityManager.getInternetInfos();
    }



    @Override
    public InternetInfo findByName (String name )
    {
        List<InternetInfo>  res =service.getInternetInfoByName(name);
        return ( res.size() == 1 ) ? res.get( 0 ) : null;
    }
    @Override
    public List<InternetInfo> getQuery()
    {
        return query;
    }
}
