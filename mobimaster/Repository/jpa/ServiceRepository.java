package Repository.jpa;

import Repository.IServiceRepository;
import model.ConnectedServices;
import model.Contact;
import model.Service;
import model.forDAO.ConnectedServicesService;
import model.forDAO.ContactService;

import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class ServiceRepository implements IServiceRepository {
    private List<ConnectedServices> query;
    private ConnectedServicesService service;


    public ServiceRepository ( ConnectedServicesService entityManager )
    {
        service = entityManager;
        query = entityManager.getConntectedServices();
    }

    @Override
    public List<ConnectedServices> getQuery()
    {
        return query;
    }

    @Override
    public ConnectedServices findByServiceList (String serviceList )
    {
        List<ConnectedServices>  res =service.getConnectedServicesByServiceList(serviceList);
        return ( res.size() == 1 ) ? res.get( 0 ) : null;
    }

}
