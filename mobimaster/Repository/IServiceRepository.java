package Repository;

import model.ConnectedServices;

import java.util.List;

public interface IServiceRepository  {

    ConnectedServices findByServiceList (String name);

    List<ConnectedServices> getQuery ();

}
