package mobimaster.exceptions;

public class URLCheckFailureExeption extends DomainLogicException
{
    public DuplicateNamedEntityException (  String url )
    {
        super( String.format( "Failed to check %s URL", url ) );
    }
}
