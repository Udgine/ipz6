package mobimaster.exceptions;

public class WrongFinalBalanceExeption extends DomainLogicException
{
    public WrongFinalBalanceExeption ( Class c )
    {
        super( String.format( "%s account balance has reached the limit", c.getName() ) );
    }
}
