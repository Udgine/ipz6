package mobimaster.exceptions;

public class ApplicationFatalError extends RuntimeException
{
    public ApplicationFatalError ()
    {
        super( "Application fatal error. Please contact your administrator." );
    }
}
