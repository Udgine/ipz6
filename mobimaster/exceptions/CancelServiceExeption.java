package mobimaster.exceptions;

public class CancelServiceExeption extends DomainLogicException
{
    public CancelServiceExeption ( Class c )
    {
        super( String.format( "%s service has been already canceled", c.getName() ) );
    }
}
