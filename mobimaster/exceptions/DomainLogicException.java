package mobimaster.exceptions;

public class DomainLogicException extends Exception
{
    public DomainLogicException ( String message )
    {
        super( message );
    }
}
