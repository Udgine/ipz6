/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package utils;

public class AbstractProperty
{

    protected AbstractProperty ( String paramName )
    {

        if ( paramName == null )
            throw new IllegalArgumentException( "paramName" );

        this.paramName = paramName;
    }

    public String getParamName ()
    {
        return this.paramName;
    }

    private String paramName;
}
