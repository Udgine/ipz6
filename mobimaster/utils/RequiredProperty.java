/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package utils;

public class RequiredProperty< T > extends AbstractProperty
{

    public RequiredProperty ( String paramName )
    {
        super( paramName );
    }

    public T getValue ()
    {
        return this.value;
    }

    public void setValue ( T value )
    {
        if ( value == null )
            throw new IllegalArgumentException( getParamName() );

        this.value = value;
    }

    private T value;

}
