/* (C) 2014-2016, Sergei Zaychenko, NURE, Kharkiv, Ukraine */

package utils;

public class RangeProperty< T extends Comparable< T > > extends AbstractProperty
{

    public RangeProperty ( String paramName, T minValue, boolean includingMin, T maxValue, boolean includingMax )
    {
        super( paramName );

        this.minValue = minValue;
        this.maxValue = maxValue;

        this.includingMin = includingMin;
        this.includingMax = includingMax;
    }

    public T getValue ()
    {
        return this.value;
    }

    public void setValue ( T value )
    {
        checkValue( value );
        this.value = value;
    }

    private void checkValue ( T value )
    {
        if ( includingMin && value.compareTo( minValue ) < 0 || !includingMin && value.compareTo( minValue ) <= 0 )
            throw new IllegalArgumentException( "Minimal range violated for " + getParamName() );

        if ( includingMax && value.compareTo( maxValue ) > 0 || !includingMax && value.compareTo( maxValue ) >= 0 )
            throw new IllegalArgumentException( "Maxiumal range violated for " + getParamName() );
    }

    private T value;
    private final T minValue, maxValue;
    private final boolean includingMin, includingMax;
}
