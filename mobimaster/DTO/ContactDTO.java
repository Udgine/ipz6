package DTO;

import java.util.Arrays;
import java.util.List;

public class ContactDTO extends DomainEntityDto<ContactDTO> {

    private String address;
    private String phone;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }
    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }

    public ContactDTO(Integer domainId, String address, String phone, Integer idAccount) {
        super(domainId);
        this.address = address;
        this.phone = phone;
        this.idAccount = idAccount;

    }

    @Override
    public String toString(){
        return String.format(
                "phone = %d\nadress = $s\nidAccount = %s\n",
                getPhone(),
                getAddress(),
                getIdAccount()
        );
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getPhone(), getAddress(), getIdAccount());
    }
}
