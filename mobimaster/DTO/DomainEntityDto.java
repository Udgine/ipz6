package DTO;

import utils.Value;
public abstract class DomainEntityDto< TConcreteDto extends Value< TConcreteDto > >
            extends Value< TConcreteDto >
    {
        protected DomainEntityDto ( Integer domainId )
        {
            this.domainId = domainId;
        }

        public Integer getDomainId ()
        {
            return domainId;
        }

        private final Integer domainId;
    }
