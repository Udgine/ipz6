package DTO;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Евгений on 20.10.2016.
 */
public class TariffStateDTO extends DomainEntityDto<TariffStateDTO> {

    private String name;
    private String costOfCall;
    private String costOfSms;
    private String costOfMms;
    private String costOfInternet;
    private Integer idAccount;

    public String getName() { return name;  }
    public String getCostOfCall() { return costOfCall;  }
    public String getCostOfSms() { return costOfSms;  }
    public String getCostOfMms() { return costOfMms;}
    public String getCostOfInternet() { return costOfInternet;  }
    public Integer getIdAccount() {return idAccount; }

    public void setName(String name) { this.name = name;}
    public void setCostOfCall(String costOfCall) { this.name = costOfCall;}
    public void setCostOfSms(String costOfSms) { this.name = costOfSms;}
    public void setCostOfMms(String costOfMms) { this.name = costOfMms;}
    public void setCostOfInternet(String costOfInternet) { this.name = costOfInternet;}




    public TariffStateDTO(Integer domainId, String name, String costOfCall, String costOfSms, String costOfMms,
            String costOfInternet, Integer idAccount) {
        super(domainId);
        this.name = name;
        this.costOfCall = costOfCall;
        this.costOfSms = costOfSms;
        this.costOfMms = costOfMms;
        this.costOfInternet = costOfInternet;
        this.idAccount = idAccount;
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList(getName());
    }
}
