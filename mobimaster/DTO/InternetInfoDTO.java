package DTO;

import java.util.Arrays;
import java.util.List;



public class InternetInfoDTO extends DomainEntityDto<InternetInfoDTO> {
    private String name;
    private String internet;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }
    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getInternet() {
        return internet;
    }
    public void setInternet(String internet) {
        this.internet = internet;
    }


    public InternetInfoDTO(Integer uuid, String name,String internet, Integer idAccount){
        super(uuid);
        this.name = name;
        this.internet = internet;
        this.idAccount = idAccount;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", name + " " + internet + ' ' + idAccount);
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getName(), getInternet(), getIdAccount());
    }
}
