package DTO;


import java.util.Arrays;
import java.util.List;

public class BalanceDTO extends DomainEntityDto<BalanceDTO> {

    private double balanceValue;
    private Integer idAccount;

    public double getBalanceValue() {
        return balanceValue;
    }
    public Integer getIdAccount() { return idAccount;}

    public void setBalanceValue(double balanceValue) {
        this.balanceValue = balanceValue;
    }

    public BalanceDTO(Integer domainId, double balanceValue, Integer idAccount) {
                super(domainId);
        this.balanceValue = balanceValue;
        this.idAccount = idAccount;
    }

    @Override
    public String toString(){
        return String.format(
                "balace = %d",
                getBalanceValue()
        );
    }

    public List< Object > getAttributesToIncludeInEqualityCheck ()
    {
        return Arrays.asList( getBalanceValue(), getIdAccount());
    }
}
