package model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class OperatorAccount extends Account{
    private List<ConnectService> connectServiceList;
    private List<ChangeTariff> changeTariffList;
    private List<AddBalance> addBalanceList;

    public List<ConnectService> getConnectServiceList() {
        return connectServiceList;
    }

    public void setConnectServiceList(List<ConnectService> connectServiceList) {
        this.connectServiceList = connectServiceList;
    }

    public List<ChangeTariff> getChangeTariffList() {
        return changeTariffList;
    }

    public void setChangeTariffList(List<ChangeTariff> changeTariffList) {
        this.changeTariffList = changeTariffList;
    }

    public List<AddBalance> getAddBalanceList() {
        return addBalanceList;
    }

    public void setAddBalanceList(List<AddBalance> addBalanceList) {
        this.addBalanceList = addBalanceList;
    }

    public OperatorAccount(Integer uuid, String name, String email, String password, Contact contact, String imageURL,
                           Tariff tariff, InternetInfo internetInfo, TariffState tariffState, ConnectedServices connectedServices, String number, Balance balance) {
        super(uuid, name, email, password, contact, imageURL, tariff, internetInfo, tariffState, connectedServices, number, balance);
        connectServiceList = new ArrayList<ConnectService>();
        changeTariffList = new ArrayList<ChangeTariff>();
        addBalanceList = new ArrayList<AddBalance>();
    }
}
