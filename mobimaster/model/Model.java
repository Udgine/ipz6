package model;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Service> serviceList;
    private List<Tariff> tariffList;
    private List<Account> accountList;
    private List<OperatorAccount> operatorAccountList;
    private List<ChangeTariff> changeTariffList;
    private List<ConnectService> connectServiceList;
    private List<AddBalance> addBalanceList;

    public List<Service> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<Service> serviceList) {
        this.serviceList = serviceList;
    }

    public List<Tariff> getTariffList() {
        return tariffList;
    }

    public void setTariffList(List<Tariff> tariffList) {
        this.tariffList = tariffList;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }

    public List<OperatorAccount> getOperatorAccountList() {
        return operatorAccountList;
    }

    public void setOperatorAccountList(List<OperatorAccount> operatorAccountList) {
        this.operatorAccountList = operatorAccountList;
    }

    public List<ChangeTariff> getChangeTariffList() {
        return changeTariffList;
    }

    public void setChangeTariffList(List<ChangeTariff> changeTariffList) {
        this.changeTariffList = changeTariffList;
    }

    public List<ConnectService> getConnectServiceList() {
        return connectServiceList;
    }

    public void setConnectServiceList(List<ConnectService> connectServiceList) {
        this.connectServiceList = connectServiceList;
    }

    public List<AddBalance> getAddBalanceList() {
        return addBalanceList;
    }

    public void setAddBalanceList(List<AddBalance> addBalanceList) {
        this.addBalanceList = addBalanceList;
    }

    public Model(){
        serviceList = new ArrayList<Service>();
        tariffList = new ArrayList<Tariff>();
        accountList = new ArrayList<>();
        operatorAccountList = new ArrayList<OperatorAccount>();
        changeTariffList = new ArrayList<ChangeTariff>();
        connectServiceList = new ArrayList<ConnectService>();
        addBalanceList = new ArrayList<AddBalance>();
    }
}
