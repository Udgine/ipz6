package model;

import java.util.UUID;

public class AddBalance extends Entity{
    private Balance balance;
    private OperationStatus operationStatus;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Balance getBalance() {
        return balance;
    }

    public void setBalance(Balance balance) {
        this.balance = balance;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public AddBalance(Integer uuid, Balance balance, String name) {
        super(uuid);
        this.balance = balance;
        this.name = name;
        operationStatus = OperationStatus.WAITING;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", "AddBalance " + name + " " + balance.getBalanceValue() + " " + operationStatus);
    }
}
