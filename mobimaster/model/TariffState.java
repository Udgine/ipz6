package model;

import utils.DomainEntity;

import java.io.Serializable;

public class TariffState  extends DomainEntity implements Serializable {
    private String name;
    private String costOfCall;
    private String costOfSms;
    private String costOfMms;
    private String costOfInternet;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCostOfCall() {
        return costOfCall;
    }

    public void setCostOfCall(String costOfCall) {
        this.costOfCall = costOfCall;
    }

    public String getCostOfSms() {
        return costOfSms;
    }

    public void setCostOfSms(String costOfSms) {
        this.costOfSms = costOfSms;
    }

    public String getCostOfMms() {
        return costOfMms;
    }

    public void setCostOfMms(String costOfMms) {
        this.costOfMms = costOfMms;
    }

    public String getCostOfInternet() {
        return costOfInternet;
    }

    public void setCostOfInternet(String costOfInternet) {
        this.costOfInternet = costOfInternet;
    }

    public TariffState(){

    }

    public TariffState(String name, String costOfCall, String costOfSms, String costOfMms, String costOfInternet){
        this.name = name;
        this.costOfCall = costOfCall;
        this.costOfSms = costOfSms;
        this.costOfMms = costOfMms;
        this.costOfInternet = costOfInternet;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", name + " " + costOfCall + " " + costOfSms + " " + costOfMms + " " + costOfInternet);
    }
}
