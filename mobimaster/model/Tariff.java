package model;

import utils.DomainEntity;

import java.io.Serializable;

public class Tariff extends DomainEntity implements Serializable {
    private String name;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Tariff(){

    }

    public Tariff(Integer uuid, String name) {
        super(uuid);
        this.name = name;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", getDomainId() + " " + name);
    }
}
