package model;

public class ConnectService extends Entity{
    private Service service;
    private OperationStatus operationStatus;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public ConnectService(Integer uuid, Service service, String name) {
        super(uuid);
        this.service = service;
        this.name = name;
        operationStatus = OperationStatus.WAITING;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", name + " " + service.getName() + " " + service.getDomainId() + " " + operationStatus);
    }
}
