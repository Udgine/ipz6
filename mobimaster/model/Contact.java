package model;

import utils.DomainEntity;

import java.io.Serializable;

public class Contact extends DomainEntity implements Serializable {
    private String address;
    private String phone;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Contact(){

    }

    public Contact(String address, String phone){
        this.address = address;
        this.phone = phone;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", address + " " + phone);
    }
}
