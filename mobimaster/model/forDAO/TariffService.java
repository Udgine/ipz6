package model.forDAO;

import DAO.ConnectionFactory;
import DAO.TariffDAO;
import model.Tariff;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class TariffService {

    private TariffDAO tariffDAO;

    public TariffService(TariffDAO tariffDAO) {
        this.tariffDAO = tariffDAO;
    }

    public void createTariff(Tariff tariff) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            tariffDAO.create(connection, tariff);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updateTariff(Tariff tariff) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            tariffDAO.update(connection, tariff);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<Tariff> getTariffByName(String name) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<Tariff> tariffs = tariffDAO.getByName(connection, name);
            connection.commit();
            return tariffs;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }


    public List<Tariff> getTariffs() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM tariffs");

        try {
            List<Tariff> accounts = tariffDAO.getTariffs(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }
}
