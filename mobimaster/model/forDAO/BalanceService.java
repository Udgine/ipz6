package model.forDAO;

import DAO.BalanceDAO;
import DAO.ConnectionFactory;
import model.Balance;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class BalanceService {

    private BalanceDAO balanceDAO;

    public BalanceService(BalanceDAO balanceDAO) {
        this.balanceDAO = balanceDAO;
    }

    public void createBalance(Balance balance) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            balanceDAO.create(connection, balance);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updateBalance(Balance balance) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            balanceDAO.update(connection, balance);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<Balance> getBalanceByValue(double value) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<Balance> balances = balanceDAO.getByValue(connection, value);
            connection.commit();
            return balances;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }
    public List<Balance> getBalances() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM balances");

        try {
            List<Balance> accounts = balanceDAO.getBalances(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }



}
