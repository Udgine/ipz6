package model.forDAO;

import DAO.ConnectionFactory;
import DAO.ContactDAO;
import model.Contact;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ContactService {

    private ContactDAO contactDAO;

    public ContactService(ContactDAO contactDAO) {
        this.contactDAO = contactDAO;
    }

    public void createContact(Contact contact) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            contactDAO.create(connection, contact);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updateContact(Contact contact) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            contactDAO.update(connection, contact);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<Contact> getContactByAddress(String address) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<Contact> contacts = contactDAO.getByAddress(connection, address);
            connection.commit();
            return contacts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }
    public List<Contact> getContacts() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM contacts");

        try {
            List<Contact> accounts = contactDAO.getContacts(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }



}
