package model.forDAO;

import DAO.AccountDAO;
import DAO.ConnectionFactory;
import model.Account;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class AccountService {

    private AccountDAO accountDAO;

    public AccountService(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    public void createAccount(Account account) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            accountDAO.create(connection, account);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public void updateAccount(Account account) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            accountDAO.update(connection, account);
            connection.commit();
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }

    public List<Account> getAccountByName(String name) throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        try {
            List<Account> accounts = accountDAO.getByName(connection, name);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }


    public List<Account> getAccounts() throws SQLException {
        Connection connection = ConnectionFactory.getConnection();
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM accounts");

        try {
            List<Account> accounts = accountDAO.getAccounts(rs);
            connection.commit();
            return accounts;
        } catch (SQLException e) {
            connection.rollback();
            throw e;
        }
    }
}
