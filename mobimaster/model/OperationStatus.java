package model;

public enum OperationStatus {
    WAITING, FINISHED
}
