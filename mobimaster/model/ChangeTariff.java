package model;

public class ChangeTariff extends Entity{
    private Tariff tariff;
    private OperationStatus operationStatus;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OperationStatus getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(OperationStatus operationStatus) {
        this.operationStatus = operationStatus;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    public ChangeTariff(Integer uuid, Tariff tariff, String name) {
        super(uuid);
        this.tariff = tariff;
        this.name = name;
        operationStatus = OperationStatus.WAITING;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", name + " " + tariff.getName() + " " + tariff.getDomainId() + " " + operationStatus);
    }
}
