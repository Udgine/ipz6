package model;

import utils.DomainEntity;

import java.io.Serializable;

public class InternetInfo  extends DomainEntity implements Serializable {
    private String name;
    private String internet;
    private Integer idAccount;

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInternet() {
        return internet;
    }

    public void setInternet(String internet) {
        this.internet = internet;
    }

    public InternetInfo(){

    }

    public InternetInfo(String name,String internet){
        this.name = name;
        this.internet = internet;
    }

    @Override
    public String toString(){
        return String.format("===%s===\n", name + " " + internet);
    }
}
