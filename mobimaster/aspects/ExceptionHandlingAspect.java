package mobimaster.aspects;

import org.springframework.core.annotation.Order;
import pizzario.exceptions.ApplicationFatalError;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import pizzario.exceptions.DomainLogicException;
import pizzario.exceptions.ServiceValidationException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;


@Component
@Aspect
@Order( 1 )
public class ExceptionHandlingAspect
{
    @Around( value = "@within(org.springframework.stereotype.Service) || @annotation(org.springframework.stereotype.Service)" )
    public Object interceptCall ( ProceedingJoinPoint joinPoint ) throws Throwable
    {
        try
        {
            return joinPoint.proceed();
        }
        catch ( DomainLogicException e )
        {
            throw e;
        }
        catch ( ConstraintViolationException e )
        {
            StringBuilder builder = new StringBuilder();
            builder.append( "Data validation error\n" );

            for ( ConstraintViolation< ? > violation : e.getConstraintViolations() )
            {
                builder.append( violation );
                builder.append( "\n" );
            }

            throw new ServiceValidationException( builder.toString() );
        }
        catch ( Exception e )
        {
            throw new ApplicationFatalError();
        }
    }
}
