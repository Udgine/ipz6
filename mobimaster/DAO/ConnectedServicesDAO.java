package DAO;

import com.mysql.jdbc.Statement;
import model.ConnectedServices;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectedServicesDAO {

    private static final String SQL_UPDATE_CONNECTEDSERVICES = "UPDATE connectedservicess SET id_account, servicelist = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_CONNECTEDSERVICES = "INSERT INTO contacts (id, id_account, servicelist) VALUES (?,?,?)";
    private static final String SQL_SELECT_BY_SERVICELIST = "SELECT * FROM connectedservicess WHERE servicelist = ?";

    public void create(Connection connection, ConnectedServices connectedServices) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_CONNECTEDSERVICES,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(connectedServices, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, ConnectedServices connectedServices) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_CONNECTEDSERVICES);
        setUpdateStatementParameters(connectedServices, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<ConnectedServices> getByServiceList(Connection connection, String servicelist) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_SERVICELIST);
        prStatement.setString(1, servicelist);
        ResultSet rs = prStatement.executeQuery();
        List<ConnectedServices> connectedServices = getConnectedServices(rs);
        prStatement.close();
        return connectedServices;
    }

    public List<ConnectedServices> getConnectedServices(ResultSet rs) throws SQLException {
        List<ConnectedServices> connectedServices = new ArrayList<ConnectedServices>();
        while (rs.next())
            connectedServices.add(getConnectedService(rs));
        return connectedServices;
    }

    public Object result (ResultSet rs, String str){
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (SQLException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return object;
    }

    private ConnectedServices getConnectedService(ResultSet rs) throws SQLException {
        ConnectedServices connectedServices = new ConnectedServices();
        connectedServices.setId(rs.getInt("id"));
        connectedServices.setIdAccount(rs.getInt("id_account"));
        connectedServices.setServiceList((ArrayList) rs.getObject("servicelist"));
        return connectedServices;

    }

    private void setUpdateStatementParameters(ConnectedServices connectedServices, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, connectedServices.getIdAccount());
        prStatement.setObject(k++, connectedServices.getServiceList());
        prStatement.setInt(k++, connectedServices.getId());
    }

    private void setInsertStatementParameters(ConnectedServices connectedServices, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, connectedServices.getId());
        prStatement.setInt(k++, connectedServices.getIdAccount());
        prStatement.setObject(k++, connectedServices.getServiceList());
    }
}
