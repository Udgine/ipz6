package DAO;

import com.mysql.jdbc.Statement;
import model.*;
import model.forDAO.*;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AccountDAO {

    private static final String SQL_UPDATE_ACCOUNT = "UPDATE accounts SET name = ?, email = ?, password = ?," +
            "imageURL = ?, contact = ?, tariff = ?, internetInfo = ?, tariffState = ?, connectedServices = ?, number = ?,  balance = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_ACCOUNT = "INSERT INTO accounts (id, name, email, password, " +
            "imageURL, contact, tariff, internetInfo, tariffState, connectedServices, number, balance)"
            + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
    private static final String SQL_SELECT_BY_NAME = "SELECT * FROM accounts WHERE name = ?";

    public void create(Connection connection, Account account) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_ACCOUNT,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(account, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, Account account) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_ACCOUNT);
        setUpdateStatementParameters(account, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<Account> getByName(Connection connection, String name) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_NAME);
        prStatement.setString(1, name);
        ResultSet rs = prStatement.executeQuery();
        List<Account> accounts = getAccounts(rs);
        prStatement.close();
        return accounts;
    }

    public List<Account> getAccounts(ResultSet rs) throws SQLException {
        List<Account> accounts = new ArrayList<Account>();
        while (rs.next())
            accounts.add(getAccount(rs));
        return accounts;
    }

    public Object result(ResultSet rs, String str) {
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        } catch (SQLException | IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return object;
    }

    private Account getAccount(ResultSet rs) throws SQLException {
        Account account = new Account();
        account.setDomainId(rs.getInt("id"));
        account.setName(rs.getString("name"));
        account.setEmail(rs.getString("email"));
        account.setPassword(rs.getString("password"));
        account.setImageURL(rs.getString("imageURL"));
        account.setContact((Contact) result(rs, "contact"));
        account.setTariff((Tariff) result(rs, "tariff"));
        account.setInternetInfo((InternetInfo) result(rs, "internetInfo"));
        account.setTariffState((TariffState) result(rs, "tariffState"));
        account.setConnectedServices((ConnectedServices) result(rs, "connectedServices"));
        account.setNumber(rs.getString("number"));
        account.setBalance((Balance) result(rs, "balance"));
        return account;

    }

    private void setUpdateStatementParameters(Account account, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setString(k++, account.getName());
        prStatement.setString(k++, account.getEmail());
        prStatement.setString(k++, account.getPassword());
        prStatement.setString(k++, account.getImageURL());
        prStatement.setObject(k++, (Contact) account.getContact());
        prStatement.setObject(k++, (Tariff) account.getTariff());
        prStatement.setObject(k++, (InternetInfo) account.getInternetInfo());
        prStatement.setObject(k++, (TariffState) account.getTariffState());
        prStatement.setObject(k++, (ConnectedServices) account.getConnectedServices());
        prStatement.setString(k++, account.getNumber());
        prStatement.setObject(k++, (Balance) account.getBalance());
        prStatement.setInt(k++, account.getDomainId());
    }

    private void setInsertStatementParameters(Account account, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, account.getDomainId());
        prStatement.setString(k++, account.getName());
        prStatement.setString(k++, account.getEmail());
        prStatement.setString(k++, account.getPassword());
        prStatement.setString(k++, account.getImageURL());
        prStatement.setObject(k++, (Contact) account.getContact());
        prStatement.setObject(k++, (Tariff) account.getTariff());
        prStatement.setObject(k++, (InternetInfo) account.getInternetInfo());
        prStatement.setObject(k++, (TariffState) account.getTariffState());
        prStatement.setObject(k++, (ConnectedServices) account.getConnectedServices());
        prStatement.setString(k++, account.getNumber());
        prStatement.setObject(k++, (Balance) account.getBalance());
    }


}
/*
    public static void main(String[] args) {
        try {
            AccountDAO accountDAO = new AccountDAO();
            AccountService accountService = new AccountService(accountDAO);
            Contact contact = new Contact("Home", "Phone");
            Tariff sTariff = new Tariff(2, "sTariff");
            Balance balance = new Balance(2, 2000);
            Service sService = new Service(2, "sService");
            ConnectedServices connectedServices = new ConnectedServices();
            connectedServices.connectService(sService);
            InternetInfo internetInfo = new InternetInfo("Internet", "200MB");
            TariffState tariffState = new TariffState("Costs", "1.50", "0.20", "0.50", "2.20");
            Account account = new Account();
            account.setId(2);
            account.setName("Name");
            account.setEmail("Email");
            account.setPassword("2");
            account.setImageURL("Image");
            account.setContact(contact);
            account.setTariff(sTariff);
            account.setInternetInfo(internetInfo);
            account.setTariffState(tariffState);
            account.setConnectedServices(connectedServices);
            account.setNumber("2");
            account.setBalance(balance);
            //accountService.createAccount(account);
            List<Account> accounts = accountService.getAccountByName("Name");
            System.out.println(accounts);
            ContactDAO contactDAO = new ContactDAO();
            ContactService contactService = new ContactService(contactDAO);
            TariffDAO tariffDAO = new TariffDAO();
            TariffService tariffService = new TariffService(tariffDAO);
            InternetInfoDAO internetInfoDAO = new InternetInfoDAO();
            InternetInfoService internetInfoService = new InternetInfoService(internetInfoDAO);
            TariffStateDAO tariffStateDAO = new TariffStateDAO();
            TariffStateService tariffStateService = new TariffStateService(tariffStateDAO);
            ConnectedServicesDAO connectedServicesDAO = new ConnectedServicesDAO();
            ConnectedServicesService connectedServicesService = new ConnectedServicesService(connectedServicesDAO);
            BalanceDAO balanceDAO = new BalanceDAO();
            BalanceService balanceService = new BalanceService(balanceDAO);
            contact.setId(2);
            contact.setIdAccount(2);
            //contactService.createContact(contact);
            sTariff.setId(2);
            sTariff.setIdAccount(2);
            //tariffService.createTariff(sTariff);
        }
        catch (SQLException e){
            e.printStackTrace();
        }
    }
}
*/