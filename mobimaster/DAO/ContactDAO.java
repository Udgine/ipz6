package DAO;

import com.mysql.jdbc.Statement;
import model.Contact;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ContactDAO {

    private static final String SQL_UPDATE_CONTACT = "UPDATE contacts SET id_account, address = ?, phone = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_CONTACT = "INSERT INTO contacts (id, id_account, address, phone) VALUES (?,?,?,?)";
    private static final String SQL_SELECT_BY_ADDRESS = "SELECT * FROM contacts WHERE address = ?";

    public void create(Connection connection, Contact contact) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_CONTACT,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(contact, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, Contact contact) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_CONTACT);
        setUpdateStatementParameters(contact, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<Contact> getByAddress(Connection connection, String address) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_ADDRESS);
        prStatement.setString(1, address);
        ResultSet rs = prStatement.executeQuery();
        List<Contact> contacts = getContacts(rs);
        prStatement.close();
        return contacts;
    }

    public List<Contact> getContacts(ResultSet rs) throws SQLException {
        List<Contact> contacts = new ArrayList<Contact>();
        while (rs.next())
            contacts.add(getContact(rs));
        return contacts;
    }

    public Object result (ResultSet rs, String str){
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (SQLException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return object;
    }

    private Contact getContact(ResultSet rs) throws SQLException {
        Contact contact = new Contact();
        contact.setId(rs.getInt("id"));
        contact.setIdAccount(rs.getInt("id_account"));
        contact.setAddress(rs.getString("address"));
        contact.setPhone(rs.getString("phone"));
        return contact;

    }

    private void setUpdateStatementParameters(Contact contact, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, contact.getIdAccount());
        prStatement.setString(k++, contact.getAddress());
        prStatement.setString(k++, contact.getPhone());
        prStatement.setInt(k++, contact.getId());
    }

    private void setInsertStatementParameters(Contact contact, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, contact.getId());
        prStatement.setInt(k++, contact.getIdAccount());
        prStatement.setString(k++, contact.getAddress());
        prStatement.setString(k++, contact.getPhone());
    }
}
