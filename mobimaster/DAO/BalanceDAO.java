package DAO;

import com.mysql.jdbc.Statement;
import model.Balance;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BalanceDAO {

    private static final String SQL_UPDATE_BALANCE = "UPDATE balances SET id_account, value = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_SQL_UPDATE_BALANCE = "INSERT INTO balances (id, id_account, value) VALUES (?,?,?)";
    private static final String SQL_SELECT_BY_VALUE = "SELECT * FROM balances WHERE value = ?";

    public void create(Connection connection, Balance balance) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_SQL_UPDATE_BALANCE,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(balance, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, Balance balance) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_BALANCE);
        setUpdateStatementParameters(balance, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<Balance> getByValue(Connection connection, double value) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_VALUE);
        prStatement.setDouble(1, value);
        ResultSet rs = prStatement.executeQuery();
        List<Balance> balances = getBalances(rs);
        prStatement.close();
        return balances;
    }

    public List<Balance> getBalances(ResultSet rs) throws SQLException {
        List<Balance> balances = new ArrayList<Balance>();
        while (rs.next())
            balances.add(getBalance(rs));
        return balances;
    }

    public Object result (ResultSet rs, String str){
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (SQLException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return object;
    }

    private Balance getBalance(ResultSet rs) throws SQLException {
        Balance balance = new Balance();
        balance.setDomainId(rs.getInt("id"));
        balance.setIdAccount(rs.getInt("id_account"));
        balance.setBalanceValue(rs.getDouble("value"));
        return balance;

    }

    private void setUpdateStatementParameters(Balance balance, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, balance.getIdAccount());
        prStatement.setDouble(k++, (float)balance.getBalanceValue());
        prStatement.setInt(k++, balance.getDomainId());
    }

    private void setInsertStatementParameters(Balance balance, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, balance.getDomainId());
        prStatement.setInt(k++, balance.getIdAccount());
        prStatement.setDouble(k++, (float)balance.getBalanceValue());
    }
}
