package DAO;

import com.mysql.jdbc.Statement;
import model.TariffState;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TariffStateDAO {

    private static final String SQL_UPDATE_TARIFFSTATE = "UPDATE tariffstates SET id_account, name = ?, costOfCall = ?, costOfSms = ?, costOfMms = ?, costOfInternet = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_TARIFFSTATE = "INSERT INTO tariffstates (id, id_account, name, costOfCall, costOfSms, costOfMms, costOfInternet) VALUES (?,?,?,?,?,?)";
    private static final String SQL_SELECT_BY_NAME = "SELECT * FROM tariffstates WHERE name = ?";

    public void create(Connection connection, TariffState tariffState) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_TARIFFSTATE,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(tariffState, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, TariffState tariffState) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_TARIFFSTATE);
        setUpdateStatementParameters(tariffState, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<TariffState> getByName(Connection connection, String name) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_NAME);
        prStatement.setString(1, name);
        ResultSet rs = prStatement.executeQuery();
        List<TariffState> tariffStates = getTariffStates(rs);
        prStatement.close();
        return tariffStates;
    }

    public List<TariffState> getTariffStates(ResultSet rs) throws SQLException {
        List<TariffState> tariffStates = new ArrayList<TariffState>();
        while (rs.next())
            tariffStates.add(getTariffState(rs));
        return tariffStates;
    }

    public Object result (ResultSet rs, String str){
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (SQLException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return object;
    }

    private TariffState getTariffState(ResultSet rs) throws SQLException {
        TariffState tariffState = new TariffState();
        tariffState.setId(rs.getInt("id"));
        tariffState.setIdAccount(rs.getInt("id_account"));
        tariffState.setName(rs.getString("name"));
        tariffState.setCostOfCall(rs.getString("costOfCall"));
        tariffState.setCostOfSms(rs.getString("costOfSms"));
        tariffState.setCostOfMms(rs.getString("costOfMms"));
        tariffState.setCostOfInternet(rs.getString("costOfInternet"));
        return tariffState;

    }

    private void setUpdateStatementParameters(TariffState tariffState, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, tariffState.getIdAccount());
        prStatement.setString(k++, tariffState.getName());
        prStatement.setString(k++, tariffState.getCostOfCall());
        prStatement.setString(k++, tariffState.getCostOfSms());
        prStatement.setString(k++, tariffState.getCostOfMms());
        prStatement.setString(k++, tariffState.getCostOfInternet());
        prStatement.setInt(k++, tariffState.getId());
    }

    private void setInsertStatementParameters(TariffState tariffState, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, tariffState.getId());
        prStatement.setInt(k++, tariffState.getIdAccount());
        prStatement.setString(k++, tariffState.getName());
        prStatement.setString(k++, tariffState.getCostOfCall());
        prStatement.setString(k++, tariffState.getCostOfSms());
        prStatement.setString(k++, tariffState.getCostOfMms());
        prStatement.setString(k++, tariffState.getCostOfInternet());
    }
}
