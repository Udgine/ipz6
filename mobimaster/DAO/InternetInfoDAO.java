package DAO;

import com.mysql.jdbc.Statement;
import model.InternetInfo;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class InternetInfoDAO {

    private static final String SQL_UPDATE_INTERNETINFO = "UPDATE internetinfos SET id_account, name = ?, internet = ? WHERE id = ? ";
    private static final String SQL_INSERT_INTO_INTERNETINFO = "INSERT INTO internetinfos (id, id_account, name, internet) VALUES (?,?,?,?)";
    private static final String SQL_SELECT_BY_NAME = "SELECT * FROM internetinfos WHERE name = ?";

    public void create(Connection connection, InternetInfo internetInfo) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_INSERT_INTO_INTERNETINFO,
                Statement.RETURN_GENERATED_KEYS);
        setInsertStatementParameters(internetInfo, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public void update(Connection connection, InternetInfo internetInfo) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_UPDATE_INTERNETINFO);
        setUpdateStatementParameters(internetInfo, prStatement);
        prStatement.executeUpdate();
        prStatement.close();
    }

    public List<InternetInfo> getByName(Connection connection, String name) throws SQLException {
        PreparedStatement prStatement = connection.prepareStatement(SQL_SELECT_BY_NAME);
        prStatement.setString(1, name);
        ResultSet rs = prStatement.executeQuery();
        List<InternetInfo> internetInfos = getInternetInfos(rs);
        prStatement.close();
        return internetInfos;
    }

    public List<InternetInfo> getInternetInfos(ResultSet rs) throws SQLException {
        List<InternetInfo> internetInfos = new ArrayList<InternetInfo>();
        while (rs.next())
            internetInfos.add(getInternetInfo(rs));
        return internetInfos;
    }

    public Object result (ResultSet rs, String str){
        Object object = null;
        try {
            byte[] array = (byte[]) rs.getObject(str);
            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array);
            ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
            object = objectInputStream.readObject();
        }
        catch (SQLException | IOException | ClassNotFoundException e){
            e.printStackTrace();
        }
        return object;
    }

    private InternetInfo getInternetInfo(ResultSet rs) throws SQLException {
        InternetInfo internetInfo = new InternetInfo();
        internetInfo.setId(rs.getInt("id"));
        internetInfo.setIdAccount(rs.getInt("id_account"));
        internetInfo.setName(rs.getString("name"));
        internetInfo.setInternet(rs.getString("internet"));
        return internetInfo;

    }

    private void setUpdateStatementParameters(InternetInfo internetInfo, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, internetInfo.getIdAccount());
        prStatement.setString(k++, internetInfo.getName());
        prStatement.setString(k++, internetInfo.getInternet());
        prStatement.setInt(k++, internetInfo.getId());
    }

    private void setInsertStatementParameters(InternetInfo internetInfo, PreparedStatement prStatement) throws SQLException {
        int k = 1;
        prStatement.setInt(k++, internetInfo.getId());
        prStatement.setInt(k++, internetInfo.getIdAccount());
        prStatement.setString(k++, internetInfo.getName());
        prStatement.setString(k++, internetInfo.getInternet());
    }
}
