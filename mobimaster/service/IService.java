﻿package service;

import DTO.ServiceDTO;

/**
 * Created by Евгений on 19.10.2016.
 */
public interface IService {

    Integer create(@NotBlank String name) throws DuplicateNamedEntityException;

    void ConnectService (@NotNull Integer servid, @NotNull Integer idNewServ);

    void CanselService (@NotNull Integer idAcc, @NotNull Integer idServ) throw CancelServiceExeption;

    ServiceDTO findServiceByName(@NotBlank String name);

}
