package service;


import DTO.*;

import java.util.List;

public interface IAccount {

    AccountDto identify (@NotBlank String name, @NotBlankString password );

    BalanceDTO showBalance(@NotBlank Integer _accId);
	
    Integer create(@NotBlank String name, @Email String email, @NotBlank String password, @NotNull ContactDTO contact, @Url String imageURL,
                   @NotNull Tariff tariff, @NotNull InternetInfoDTO internetInfo, @NotNull TariffStateDTO tariffState, @NotNull ConnectedServicesDTO connectedServices, @Phone String number, @Balance BalanceDTO balance) throws DuplicateNamedEntityException;

    List<ServiceDTO> ShowConnectedServices(@NotBlank Integer _accId);

    TariffDTO ShowTariffDetails(@NotBlank Integer _tarId);

    void addBalance ( @NotBlank Integer _balId, @Balance double balance ) throw WrongFinalBalanceExeption;

    void uploadImage (@NotBlank Integer _accId,@Url String url ) throw URLCheckFailureExeption;

    ContactDTO viewContactDetails(@NotBlank Integer _contId);

    void editContactDetails ( @NotBlank Integer _contId, @NotBlank String newAddress, @Phone String newPhone );

    TariffDTO viewTariffDetails(@NotBlank Integer _tarId);

    ServiceDTO viewServiceDetails(@NotBlank Integer _servId);

    List<TariffDTO> viewUnconfirmedTariffs(@NotBlank Integer _accId);
	
    void changeTariff(@NotBlank Integer _accId, @NotBlank Integer _tarId);

}
