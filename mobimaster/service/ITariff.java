package service;

import DTO.TariffDTO;

import java.util.List;

/**
 * Created by Евгений on 19.10.2016.
 */
public interface ITariff {

    List<TariffDTO> viewAllTariffs();
	
	public Integer create ( @NotBlank String name )throws DuplicateNamedEntityException;

    TariffDTO findTariffByName(@NotBlank String name);
	
	void changeTariff(String name, String tariffName);
	
	TariffDTO viewTariffDetails(String name);

}
