﻿package service;

import DTO.BalanceDTO;

import java.util.List;

/**
 * Created by Евгений on 19.10.2016.
 */
public interface IBalance {

    

	Integer create(@Balance double value);
	
    List<BalanceDTO> ViewBalanceChangeDetails(@NotNull Integer _accId);


}
