package service.impl;

import DTO.*;
import model.*;

import java.util.ArrayList;
import java.util.List;

public final class DtoBuilder
{

    public static AccountDto toDto ( Account account ) {
        return new AccountDto(
                account.getDomainId(),
                account.getName(),
                account.getEmail(),
                account.getPassword(),
                account.getImageURL(),
                DtoBuilder.toDto(account.getContact()),
                DtoBuilder.toDto(account.getInternetInfo()),
                DtoBuilder.toDto(account.getTariffState()),
                account.getNumber(),
                DtoBuilder.toDto(account.getBalance()));
    }

    public static BalanceDTO toDto (Balance balance)
    {
        return new BalanceDTO(balance.getDomainId(), balance.getBalanceValue(), balance.getIdAccount());
    }

    public static ContactDTO toDto (Contact contact)
    {
        return new ContactDTO(contact.getDomainId(),contact.getAddress(), contact.getPhone(), contact.getIdAccount());
    }

    public  static InternetInfoDTO toDto (InternetInfo internetInfo)
    {
        return new InternetInfoDTO(internetInfo.getDomainId(),internetInfo.getName(), internetInfo.getInternet(),
                internetInfo.getIdAccount());
    }

    public  static ServiceDTO toDto (Service service)
    {
        return new ServiceDTO(service.getDomainId(), service.getName());
    }

    public static TariffDTO toDto(Tariff tariff)
    {
        return new TariffDTO( tariff.getDomainId(), tariff.getName(), tariff.getIdAccount());
    }

    public static  TariffStateDTO toDto( TariffState tariffStateDTO)
    {
        return new TariffStateDTO(tariffStateDTO.getDomainId(), tariffStateDTO.getName(), tariffStateDTO.getCostOfCall(),
                tariffStateDTO.getCostOfSms(), tariffStateDTO.getCostOfMms(),
                tariffStateDTO.getCostOfInternet(), tariffStateDTO.getIdAccount());
    }



}