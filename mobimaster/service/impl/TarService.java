﻿package service.impl;

import DTO.TariffDTO;
import Repository.jpa.TariffRepository;
import model.Tariff;
import service.ITariff;

import java.util.List;

/**
 * Created by Евгений on 21.10.2016.
 */
public class TarService implements ITariff{

    private TariffRepository tariffRepository;
	
	
	private Random random;

    public TarService(TariffRepository tariffRepository, Random random)
    {
        this.tariffRepository = tariffRepository;
		this.random = random;
    }

    @Override
    public List<TariffDTO> viewAllTariffs()
    {
        List<Tariff> tariffList = tariffRepository.getQuery();
        List<TariffDTO> res = null;
        for (int i=0; i < tariffList.size(); i++)
        {
            res.add(DtoBuilder.toDto(tariffList.get(i)));
        }
        return  res;
    }
	
	@Override
    public Integer create ( String name )
    {
        if ( tariffRepository.findByName( name ) != null )
            throw new IllegalStateException( "Duplicate tariff " + name );

        Tariff tariff = new Tariff( , name );

        tariffRepository.getQuery.add(tariff);

        return tariff.getUUID();
    }

    @Override
    public TariffDTO findTariffByName(String name)
    {
       return DtoBuilder.toDto(tariffRepository.findByName(name));
    }

	
}

