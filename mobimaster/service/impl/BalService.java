﻿package service.impl;

import DTO.BalanceDTO;
import Repository.IAccountRepository;
import Repository.jpa.BalanceRepository;
import model.Account;
import model.Balance;

import java.util.List;

/**
 * Created by Евгений on 21.10.2016.
 */
public class BalService {

    private IAccountRepository accountRepository;
    private BalanceRepository balanceRepository;

    public BalService(BalanceRepository balanceRepository, IAccountRepository accountRepository)
    {
        this.accountRepository = accountRepository;
        this.balanceRepository = balanceRepository;
    }


    List<BalanceDTO> ViewBalanceChangeDetails(String name)
    {
        Account acc = accountRepository.findByName(name);
        List<BalanceDTO> res = null;
        List<Balance> data = balanceRepository.getQuery();
        for (int i = 0; i < data.size(); i++ )
        {
            if (data.get(i).getIdAccount() == acc.getDomainId())
            {
                res.add(DtoBuilder.toDto(data.get(i)));
            }
        }

        return res;

    }



}
