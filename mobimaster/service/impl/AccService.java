package service.impl;

import DTO.*;
import Repository.IAccountRepository;
import Repository.IBalanceRepository;
import Repository.IContactRepository;
import Repository.IServiceRepository;
import Repository.jpa.TariffRepository;
import model.*;
import model.forDAO.ConnectedServicesService;
import service.IAccount;

import java.util.List;

/**
 * Created by Евгений on 21.10.2016.
 */
public class AccService implements IAccount {

    private IAccountRepository accountRepository;
    private IBalanceRepository balanceRepository;
    private IServiceRepository serviceRepository;
    private TariffRepository tariffRepository;
    private IContactRepository contactRepository;
	private Random random;

    public AccService(IAccountRepository accountRepository, IBalanceRepository balanceRepository,
                      IServiceRepository serviceRepository, TariffRepository tariffRepository,
                      IContactRepository contactRepository, Random random

    ) {

        this.accountRepository = accountRepository;
        this.balanceRepository = balanceRepository;
        this.serviceRepository = serviceRepository;
        this.tariffRepository = tariffRepository;
        this.contactRepository = contactRepository;
		this.random = random;
    }

    @Override
    public AccountDto identify(String name, String password) {

        Account a = accountRepository.findByName(name);
        if (a == null)
            return null;

        if (!a.checkPassword(password))
            return null;

        return new AccountDto(a.getDomainId(),
                a.getName(),
                a.getEmail(),
                a.getPassword(),
                a.getImageURL(),
                DtoBuilder.toDto(a.getContact()),
                DtoBuilder.toDto(a.getTariff()),
                DtoBuilder.toDto(a.getInternetInfo()),
                DtoBuilder.toDto(a.getTariffState()),
                a.getNumber(),
                DtoBuilder.toDto(a.getBalance()));
    }

    @Override
    public BalanceDTO showBalance(Integer _accId) {
        return DtoBuilder.toDto(this.balanceRepository.find(_accId));
    }

	@Override
	public Integer create(String name, String email, String password, ContactDTO contact, String imageURL,
                   TariffDTO tariff, InternetInfoDTO internetInfo, TariffStateDTO tariffState, ConnectedServicesDTO connectedServices, String number, BalanceDTO balance){
        if ( accountRepository.findByName( name ) != null )
            throw new IllegalStateException( "Duplicate account " + name );

        Account account = new Account(Random.nextInt(100), name, email, password, contact, imageURL,
                   tariff, internetInfo, tariffState, connectedServices, number, balance);

        accountRepository.getQuery.add(account);

        return account.getUUID();
    }			   
	
    @Override
    public List<ServiceDTO> ShowConnectedServices(Integer _accId)
    {
        Account acc = accountRepository.find(_accId);
        List<ServiceDTO> res = new ArrayList<ServiceDTO>;
        List<Service> data = acc.getConnectedServices().getServiceList();
        for (int i=0; i<data.size(); i++)
        {
            res.add(DtoBuilder.toDto(data.get(i)));
        }
        return  res;
    }

    @Override
    public TariffDTO viewTariffDetails(String name)
    {
        Account acc = accountRepository.findByName(name);
        return DtoBuilder.toDto(acc.getTariff());
    }


    @Override
    public void addBalance(Integer _balId, double value)
    {
        Balance bal = balanceRepositoryRepository.find(_balId);
        bal.setBalanceValue(bal.getBalanceValue() + value);
    }


    @Override
    public void uploadImage(Integer _accId, String url)
    {
        Account acc = accountRepository.find(_accId);
        acc.setImageURL(url);

    }


    @Override
    public ContactDTO viewContactDetails(Integer _contId)
    {
        return DtoBuilder.toDto(contactRepository.find(_contId));
    }

    @Override
    public void editContactDetails(Integer _contId, String newAdress, String newPhone) {

        Contact contact = contactRepository.find(_contId);
        contact.setPhone(newPhone);
        contact.setAddress(newAdress);

    }

    @Override
    public TariffDTO viewTariffDetails(Integer _tarId) {
        Account acc = accountRepository.find(_tarId);

        return DtoBuilder.toDto(acc.getTariff());
    }

    @Override
    public List<TariffDTO> viewUnconfirmedTariffs(Integer _accId) {
        Account acc = accountRepository.find(_accId);
        List<TariffDTO> res = new ArrayList<TariffDTO>;
        List<Tariff> data = tariffRepository.getQuery();

        for (int i = 0; i < data.size(); i++ )
        {
            if ( data.get(i).getName() != acc.getTariff().getName())
                res.add(DtoBuilder.toDto(data.get(i)));
        }

        return res;
    }

    @Override
    public ServiceDTO viewServiceDetails(Integer _servId)
    {
        List<ConnectedServices> data = serviceRepository.getQuery();
        for (int i=0; i < data.size(); i++)
        {
                List<Service> data2 = data.get(i).getServiceList();
                for (int j = 0; j < data2.size(); j++) {
                    if (data2.get(j).getId() == _servId)
                        return DtoBuilder.toDto(data2.get(j));
            }

        }
        return null;
    }

  
    @Override
    public void changeTariff(Integer _accId,Integer _tarId)

    {
        Account acc = accountRepository.find(_accId);
        Tariff tar = tariffRepository.find(_tarId);

        acc.setTariff(tar);
    }






}
