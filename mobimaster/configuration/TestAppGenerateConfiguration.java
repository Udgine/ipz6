package mobimaster.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan( basePackages = "mobimaster.testapp" )
public class TestAppGenerateConfiguration extends RootConfiguration {

    @Override
    protected String getPersistenceUnitName() {
        return "MobimasterCreate";
    }
}
