package mobimaster.configuration;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.*;
import org.springframework.core.Ordered;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;


@Configuration
@ComponentScan( basePackages = { "mobimaster.service.impl", "mobimaster.repository.jpa", "mobimaster.aspects" })
@EnableTransactionManagement(
        mode = AdviceMode.PROXY, proxyTargetClass = false,
        order = Ordered.LOWEST_PRECEDENCE
)
@EnableAspectJAutoProxy( proxyTargetClass = false )
public abstract  class RootConfiguration {

    protected abstract String getPersistenceUnitName ();

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean ()
    {
        HibernateJpaVendorAdapter adapter = new HibernateJpaVendorAdapter();
        adapter.setDatabasePlatform( "org.hibernate.dialect.DerbyDialect" );

        LocalContainerEntityManagerFactoryBean factory =
                new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter( adapter );
        factory.setPersistenceUnitName( getPersistenceUnitName() );
        return factory;
    }

    @Bean
    public PlatformTransactionManager jpaTransactionManager()
    {
        return new JpaTransactionManager(
                this.entityManagerFactoryBean().getObject()
        );
    }

    @Bean
    public LocalValidatorFactoryBean localValidatorFactoryBean()
    {
        LocalValidatorFactoryBean validator = new LocalValidatorFactoryBean();
        validator.setProviderClass( HibernateValidator.class );
        return validator;
    }

    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor ()
    {
        MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
        processor.setValidator( this.localValidatorFactoryBean() );
        return processor;
    }
}
