package testapp;

import DAO.AccountDAO;
import model.*;
import model.forDAO.AccountService;

import java.util.Random;
import java.util.UUID;

public class ModelGenerator {

    private Service fService, sService, tService;
    private Tariff fTariff, sTariff, tTariff;
    private Account account;
    private Account operatorAccount;
    private Random random;
    public ModelGenerator(){
        random = new Random();

    }
    public void generateData(){
        generateServices();
        generateTariffs();
        generateAccounts();
        generateOperatorAccounts();
        generateChangeTariff();
        generateConnectService();
        generateAddBalance();

    }

    private void generateServices(){
		ServService servService = new ServService(serviceDAO, serviceService);
        fService = servService.create("fService");
        sService = servService.create("sService");
        tService = servService.create("tService");
    }

    private static void generateTariffs(){
		TarService tarService = new TarService(tariffDAO, tariffService);
        fTariff = tarService.create("fTariff");
        sTariff = tarService.create("sTariff");
        tTariff = tarService.create("tTariff");
    }

    private void generateAccounts(){
        AccountSer accountSer = new AccountSer(accountDAO, accountService);
        accountSer.create("Name", "Email", "2", "Image","Home", "Phone",
                 "sTariff", "Internet", "200MB", "Costs", "1.50", "0.20" , "0.50", "2", "2,0");
    }

    private static void generateBalances(){
        BalService balService = new BalService(balanceDAO, balanceService);
		Balance balance = new Balance(random.nextInt(100), "2,0");
    }
}
