package testapp;

import model.Model;

import java.io.PrintStream;
import java.util.List;

public class ModelState {
    private PrintStream printStream;

    public ModelState(PrintStream printStream){
        this.printStream = printStream;
    }


    public void modelView(ModelGenerator model){
        modelPrint("Services", model.getServiceList());
        modelPrint("Tariffs",model.getTariffList());
        modelPrint("Accounts",model.getAccountList());
        modelPrint("Balances",model.getAddBalanceList());
    }

    public void modelPrint(String name, List list){
        printStream.format("===%s===\n", name);

        for (Object object : list) {
            printStream.print(object);
        }
    }
}
